duplocator
========

## Table of contents

- [Description](#description)
- [Documentation](#documentation)

## Description
--------------

In certain cases of unmaintaned backups or "manual" backups, a filesystem can contain
many files and directories that are either duplicates or very similar in contents.

duplocator aims at digging through the filesystem, and not only analyze files for
duplicates, but find any full directory trees that might be either duplicate or
similar in structure. 

What makes duplocator different from other directory merging software? 
Well not much actually. Some program use strictly checksum comparisons,
others use file-size comparisons, and still others use both and more methods.

Mainly, this project is more of a "learn c++ by writing a program" project. 
So, we will see how this goes!

## Documentation
---------------



