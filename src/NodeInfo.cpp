/*
 * File: NodeInfo.cpp 
 * Author: ruskiyos
 *
 */

#include "NodeInfo.hpp"
#include "DirInfo.hpp"

// find()
#include <algorithm>

NodeInfo::NodeInfo(string name, string path) {
    this->name = name;
    this->parent = NULL;
    this->size = 0;
    this->modifyTime = 0;
    this->file = true;
    this->starred = false;
    this->refresh = false;

    // Manually make path because no parent node
    this->path = path+"/"+name;
}

NodeInfo::NodeInfo(string name, DirInfo* parent) {
    this->name = name;
    this->parent = parent;
    this->size = 0;
    this->modifyTime = 0;
    this->file = true;
    this->starred = false;
    this->refresh = false;

    // Automaticall generate path
    this->updatePath();
    
}

string NodeInfo::getName() {
    return this->name;
}

void NodeInfo::setName(string name) {
    this->name = name;
}

int NodeInfo::getSize() {
    return this->size;
}

//TODO calculate directory size
void NodeInfo::setSize(int size) {
    this->size = size;
}

void NodeInfo::addSize(int size) {
    this->size += size;
}

string NodeInfo::getPath() {
    return path;
}

string NodeInfo::getFullPath() {
    if (parent == NULL)
        return path;
    else
        return parent->getFullPath()+"/"+name;
}

void NodeInfo::updatePath() {
    path = this->getFullPath();
}

DirInfo* NodeInfo::getParent() {
    return this->parent;
}

void NodeInfo::setParent(DirInfo* parent) {
    this->parent = parent;
}

bool NodeInfo::isAncestor(DirInfo* parent) {
    if (this->parent == NULL) {
        return false;
    } else if (this->parent == parent) {
        return true;
    } else {
        return this->parent->isAncestor(parent);
    }
}

bool NodeInfo::isFile() {
    return this->file;
}

void NodeInfo::isFile(bool isFile) {
    this->file = isFile;
}

bool NodeInfo::isDir() {
    return !this->file;
}

void NodeInfo::isDir(bool isDir) {
    this->file = !isDir;
}

bool NodeInfo::isStarred() {
    return this->starred;
}

void NodeInfo::isStarred(bool starred) {
    this->starred = starred;
}

bool NodeInfo::isHidden() {
    if (this->name != "." &&
        this->name != ".." &&
        this->name[0] == '.') {
        return true;
    } else {
        return false;
    }
}

bool NodeInfo::getRefresh() {
    return this->refresh;
}

void NodeInfo::setRefresh(bool refresh) {
    this->refresh = refresh;
}

int NodeInfo::getModifyTime() {
    return this->modifyTime;
}

void NodeInfo::setModifyTime(int time) {
    this->modifyTime = time;
}

void NodeInfo::addSimilar(NodeInfo* node) {
    if ( this->findSimilar(node) == -1 ) {
        this->similar.push_back(node);
    }

}

void NodeInfo::removeSimilar(NodeInfo* node) {
    int pos = this->findSimilar(node);

    if( pos != -1 ) {
        this->similar.erase(similar.begin() + pos);
    }

}

int NodeInfo::findSimilar(NodeInfo* node) {
    vector<NodeInfo*>::iterator it = find(similar.begin(), similar.end(), node);

    if (it != similar.end())
        return it - similar.begin();
    else
        return -1;

}

int NodeInfo::getNumSimilar() {
    return this->similar.size();
}

vector<NodeInfo*> NodeInfo::getSimilar() {
    return this->similar;
}

