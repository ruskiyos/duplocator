/* 
 * File: FileSystem.cpp
 * Author: ruskiyos
 *  
 */

#include "FileSystem.hpp"
#include "NodeInfo.hpp"
#include "FileInfo.hpp"
#include "DirInfo.hpp"

#include <iostream>


#include <boost/filesystem.hpp>

/**
  * FileSystem
  * 
  * if path exists, initialize FileSystem class
  * 
  * @param path root node to analyze
  */
FileSystem::FileSystem(string path) {
    // Display debug messages 
    debug = true;

    // Create a boost path object
    boost::filesystem::path p(path);

    // Counts number of duplicate directories discovered    
    this->dupDirCount = 0;

    try {
        // does path exist
        if( exists(p) ) {
            // is path a directory
            if( is_directory(p) ) {
                try {
                    DirInfo* node = new DirInfo( 
                        p.filename().string(), 
                        p.parent_path().string() 
                    );
                    node->isDir(true);

                    debugPrint("root path = "+node->getPath());

                    // Keeps track of node to deconstruct pointer
                    allNodes.push_back(node);
                    // All directories list (for quick dir operations)
                    allDirs.push_back(node);

                    // Spefiy filesystem root and path
                    root = node;
                    path = node->getPath();
                    //TODO Might remove...
                    depth = 0;

                } catch (bad_alloc&) {
                    throw "EXPN_BAD_NODE";
                }
            } else {
                cout << "Error: Not a valid directory"; 
                throw "EXPN_INVALID_ROOT";
            }
        } else {
            cout << "Error: " << p << " does not exist." << endl;
            throw "EXPN_PATH_DNE";
        }

    } catch ( const boost::filesystem::filesystem_error& ex ) {
        cout << ex.what() << endl;   
    }

}

/**
  * ~FileSystem
  *
  * deconstructor cleans up all pointers
  */
FileSystem::~FileSystem() {
    unsigned int ix = 0;

    // All node pointers are in "addNodes" vector
    // allNodes contains all pointers in allDirs and allFiles as well
    for(ix = 0; ix < allNodes.size(); ++ix) {
        delete allNodes.at(ix);
    }
}

/**
  * getRoot
  * 
  * returns the set FileSystem root
  *
  * @return a DirInfo pointer to FS root
  */
DirInfo* FileSystem::getRoot() {
    return this->root;
}


/**
  * setRoot
  * 
  * sets a new root for the FS class and updates the path
  *
  * @param node pointer to a DirInfo object
  */
void FileSystem::setRoot(DirInfo* node) {
    this->root = node;
    this->path = node->getPath();
    this->depth = 0;
}

/**
  * scan
  * 
  * overloaded function to scan the root node
  * in case no node is specified
  */
bool FileSystem::scan() {
    scan(root);

    return true;    
}

/**
  * scan
  * 
  * starts from the given node and iterates through all
  * child directories and builds vectors of all file
  * and directory nodes in the FS
  * 
  * @param pnode pointer to DirInfo node to scan
  */
bool FileSystem::scan(DirInfo* pnode) {
    debugPrint("Scanning Node: " + pnode->getPath());

    // Check if given path exists
    if ( !boost::filesystem::exists( pnode->getPath() ) ) {
        cout << pnode->getPath() << " doesn't exist" << endl;
        return false;
    }

    if (pnode->isHidden()) {
        return false;
    }

    // Create a boost directory iterator
    boost::filesystem::directory_iterator end_itr;
    try {

    for ( boost::filesystem::directory_iterator itr( pnode->getPath() ); 
          itr != end_itr; 
          ++itr ) {

        // Is iterated object a directory
        if ( is_directory(itr->status()) ) {

            DirInfo* dir = new DirInfo(itr->path().filename().string(), pnode);

            // Keeps track of node to deconstruct pointer
            allNodes.push_back(dir);
            // All directories list (for quick dir operations)
            allDirs.push_back(dir);
            // Add this node to it's parent children list
            pnode->addChild(dir);

            // Create a boost path object to extract fs info
            boost::filesystem::path p(dir->getPath());
            dir->setModifyTime( last_write_time(p) );
            dir->isDir(true);

            // Recursively scan dirs
            scan(dir);

            // directory size is only available after all dir and files add their
            // sizes to their parent dirs to be added here
            pnode->addSize(dir->getSize());

        // If iterated object is not directory, then it must be a file
        } else {

            FileInfo* file = new FileInfo(itr->path().filename().string(), pnode);

            // Keeps track of node to deconstruct pointer
            allNodes.push_back(file);
            // All files list (for quick file operations)
            allFiles.push_back(file);
            // Add this node to it's parent children list
            pnode->addChild(file);

            // Create a boost path object to extract fs info
            boost::filesystem::path p(file->getPath());
            try {
                file->setSize( file_size(p) );
            } catch (boost::filesystem::filesystem_error err) {
                file->setSize( 0 );
            }

            try {
                file->setModifyTime( last_write_time(p) );
            } catch (boost::filesystem::filesystem_error err) {
                file->setModifyTime( 0 );
            }
            // Update parent size
            file->getParent()->addSize(file->getSize());

        }
    }
    } catch (boost::filesystem::filesystem_error) {
        cout << "Cannot open file: " << pnode->getPath() << ". Permission denied!" << endl;
    }
 
    return true;
}

/**
  * moveChild
  * 
  * takes a NodeInfo (FileInfo or DirInfo) node
  * and moves it from its current parent to
  * the new specified parent (changes pointers)
  *
  * @param child the child node that is being moved
  * @param parent the parent node under which the
  *     child node is moving 
  */
bool FileSystem::moveChild(NodeInfo* child, DirInfo* parent) {
    child->getParent()->removeChild(child);
    parent->addChild(child);
    child->setParent(parent);
    child->updatePath();

    return true;
}

/**
  * binarySearch
  * 
  * uses a binary search algorithm to find a 
  * node in a vector
  * 
  */
int FileSystem::binarySearch(vector<NodeInfo*> nodes, NodeInfo* node, int min, int max) {

    int mid = ( min + max ) / 2;
    int sMid = nodes.at(mid)->getSize();
    int sNode = node->getSize();

    if (min == max && min == mid) { // is second condition necessary?
        int sMin = nodes.at(min)->getSize();

        if (sNode < sMin) {
            return min;
        } else {
            return max + 1;
        }
    } else if (sNode < sMid) {
        return binarySearch(nodes, node, min, mid - 1);
    } else if (sNode > sMid) {
        return binarySearch(nodes, node, mid + 1, max);
    } else {
        return sMid + 1;
    }

    return 0;
}

/**
  * insertionSort
  * 
  * uses a insertion sort algorithm to insert a new node
  * into a vector
  *
  */
void FileSystem::insertionSort(vector<NodeInfo*> nodes) {

    unsigned int ix = 0,
                 jx = 0;

    int nSize = 0;

    for (ix = 1; ix < nodes.size() - 1; ++ix) {

        nSize = allNodes.at(ix)->getSize();

        jx = ix - 1;
        NodeInfo* cmp = allNodes.at(jx);
        while( jx >= 0 && nSize < cmp->getSize() ) {
            
        }
    }

}

/**
  * sortParam_Size
  *
  * function used by the sort function to sort two
  * nodes by size
  */
bool FileSystem::sortParam_Size(NodeInfo* a, NodeInfo* b) {
    return a->getSize() < b->getSize();
}

/**
  * sortParam_Name
  *
  * function used by the sort function to sort two
  * nodes by naeme
  */
bool FileSystem::sortParam_Name(NodeInfo* a, NodeInfo* b) {
    return a->getName() < b->getName();
}

/**
  * sortParam_ModifyTime
  *
  * function used by the sort function to sort two
  * nodes by modify time
  */
bool FileSystem::sortParam_ModifyTime(NodeInfo* a, NodeInfo* b) {
    return a->getModifyTime() < b->getModifyTime();
}

/**
  * analyze
  * 
  * finds all duplicate/similar directories
  * 
  */
bool FileSystem::analyze() {
    // DEBUG Test sorting algorithms
    /*
    debugPrint("\nOriginal Dirs");
    debugPrintNodes(allDirs);

    sort(allDirs.begin(), allDirs.end(), sortParam_Name);

    debugPrint("Sorted Dirs (name)");
    debugPrintNodes(allDirs);

    debugPrint("\nOriginal Files (size)");
    debugPrintNodes(allFiles);

    sort(allFiles.begin(), allFiles.end(), sortParam_Size);

    debugPrint("Sorted Files");
    debugPrintNodes(allFiles);
    

    debugPrint("Sample CRC: "); 
    debugPrint(allFiles.at(2)->getChecksum());
    debugPrint(allFiles.at(3)->getChecksum());
    */

    // Analyze directories
    vector<DirInfo*>::iterator it;
    vector<DirInfo*>::iterator temp;

    for(it = allDirs.begin(); it != allDirs.end(); ++it) {
        for(temp = it + 1; temp != allDirs.end(); ++temp) {
            // If this dir is already in similar list, skip
            if ((*it)->findSimilar(*temp) != -1) {
                continue;

            // If a dir has a similar file, and is not a child of that dir
            } else if ((*it)->similarTo(*temp) && !(*it)->isAncestor(*temp)) {
                // If a is similar to b, then b is similar to a
                (*it)->addSimilar(*temp);
                (*temp)->addSimilar(*it);
                this->dupDirCount += 1;
            }

            /* Alternate method
            DirInfo* dir = *it;
            DirInfo* tempDir = *temp;
            if (dir->findSimilar(tempDir) != -1) {
                break;
            } else if (dir->similarTo(tempDir)) {
                dir->addSimilar(tempDir);
            } 
            */
             

        }
    }

    return true;
}

void FileSystem::displayResults() {
    vector<DirInfo*>::iterator it;
    DirInfo* dir;

    cout << "Directory Tree: " << endl;
    displayTree(0,this->root);



    for(it = allDirs.begin(); it != allDirs.end(); ++it) {
        dir = (*it);
        if( dir->getNumSimilar() > 0 ) {
            dir->printInfo();
           
            /*
            // Marks for diplicate listing
            dir->isStarred(true); 
            displayTree(0,this->root);
            // Un-Marks in case of no diplicate listing
            dir->isStarred(false);
            */
        }

    }

}

void FileSystem::displayTree(int indent, NodeInfo* node) {
    int tabspace = 2;
    vector<NodeInfo*>::iterator it;
    vector<NodeInfo*> cnodes;

    // Insert tabs before print
    cout << string(indent*tabspace,' ');
    if ( node->isStarred() ) {
        cout << "[~" << node->getName() << "~]" << endl; 
        node->isStarred(false);
    } else {
        cout << "[" << node->getName() << "]" << endl; 
    }

    // Increase indent
    ++indent;

    cnodes = node->getChildren();
    for(it = cnodes.begin(); it != cnodes.end(); ++it) {
        
        if( (*it)->isDir() ) {
            displayTree(indent, (*it));
        } else {
            //cout << string(" ",indent*tabspace) << (*it)->getName() << endl; 
            cout << string(indent*tabspace,' ');

            if ( (*it)->isStarred() ) {
                cout << "~" << (*it)->getName() << "~" << endl; 
                (*it)->isStarred(false);
            } else {
                cout << (*it)->getName() << endl; 
            }
        } 
    }
}

/**
  * debugPrint
  * 
  * used to print debug messages to the console only when
  * the "debug" variable is set to true
  *
  * @param mesg the message to print to user
  */
template<class Mesg> void FileSystem::debugPrint(Mesg mesg) {
    if( debug ) {
        cout << mesg << endl;
    }
}

template<class NodeVector> void FileSystem::debugPrintNodes(NodeVector nodes) {
    if( debug) {
        unsigned int ix;
        NodeInfo* node;

        for (ix = 0; ix < nodes.size(); ++ix) {
            node = nodes.at(ix);

            cout << "Node: " << node->getName()
                << "\t\tSize: " << node->getSize()
                << "\tModify: " << node->getModifyTime()
                << "\tPath: " << node->getPath()
                << "\tSimilars: ";
        
            vector<NodeInfo*>::iterator it;
            vector<NodeInfo*> nodes = node->getSimilar();
            for(it=nodes.begin(); it != nodes.end(); ++it) {
                cout << (*it)->getPath() << ", ";
            }

            cout << endl;
        }
    }

}
