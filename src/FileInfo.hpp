/*
 * File: FileInfo.hpp 
 * Author: ruskiyos
 *
 */

#ifndef FILEINFO_HPP
#define FILEINFO_HPP

// Forward declarations
class NodeInfo;
class DirInfo;

#include <string>

#include "NodeInfo.hpp"

using namespace std;

// FileInfo extends NodeInfo
class FileInfo : public NodeInfo {

    protected:
        long int checksum;

    public:
        FileInfo(string name, string path);
        FileInfo(string name, DirInfo* parent);
        int getChecksum();
        void calcChecksum();
};

#endif /* FILEINFO_HPP */

