/*
 * File: NodeCompare.hpp 
 * Author: ruskiyos
 *
 */

#ifndef NODECOMPARE_HPP
#define NODECOMPARE_HPP

// Forward declarations
class NodeInfo;

enum Status { 
    Equal, 
    Similar,
    Different
};

enum ScanType {
    Quick,
    Deep
};

enum NodeSelect {
    First,
    Second,
    Both
};


#include <vector>

using namespace std;

class NodeCompare {

    private:
        NodeInfo* node1;
        NodeInfo* node2;

        vector<NodeInfo*> diffNode1;
        vector<NodeInfo*> diffNode2;
        vector<NodeInfo*> sameNode;

        Status status;
        ScanType type;
        
    public:
        NodeCompare(NodeInfo* node1, NodeInfo* node2, ScanType type);
        vector<NodeInfo*> getNodeDiff(NodeSelect select);
        void addNodeDiff(NodeSelect select, NodeInfo* diff);
        Status getStatus();
        void setStatus(Status stat);
        ScanType getScanType();
        void setScanType(ScanType type);
};

#endif /* NODECOMPARE_HPP */

