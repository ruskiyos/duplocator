/* 
 * File: duplocator.cpp
 * Author: ruskiyos
 * Project: duplocator
 *
 */

// cin
#include <iostream>
#include <string>

#include "FileSystem.hpp"

using namespace std;

// Function Forward Declarations
void printIntro();

int main() {

    string dir;
    /* Non-debug use

    // Put user fluff outside main function
    printIntro();

    cout << "Enter a starting directory: ";
    
    if( !getline(cin, dir) ) {
        cout << "\nError: I/O error!" << endl;
        return -1;
    }

    if( !dir.empty() ) {
        FileSystem fs(dir);

        fs.scan();
    } else {
        cout << "\nError: Please enter a valid dir path" << endl;
    }
    */

    // Testing directory
    //dir = "./TestCases/test1";
    dir = "./TestCases/test2";

    // Create FileSystem object with specified directory
    FileSystem fs(dir);

    // Scan and build a dir and file vector for the filesystem
    fs.scan();

    // Find duplicate/similar directories
    fs.analyze();

    // Print information to user
    fs.displayResults();

} // main

void printIntro() {
    cout << "*************************************" << endl;
    cout << "*************************************" << endl;
    cout << "Welcome to duplocator!" << endl;
    cout << "Too many duplicate, scattered files in your "
         << "file system? I can try to clean that up "
         << "for you so that you don't have to." << endl;
}
