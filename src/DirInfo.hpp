/*
 * File: DirInfo.hpp 
 * Author: ruskiyos
 *
 */

#ifndef DIRINFO_HPP
#define DIRINFO_HPP

// Forward declarations
class NodeInfo;

#include <string>
#include <vector>

#include "NodeInfo.hpp"

using namespace std;

// DirInfo extends NodeInfo
class DirInfo : public NodeInfo {

    protected:
        int depth;
        vector<NodeInfo*> children;

    public:
        DirInfo(string name, string path);
        DirInfo(string name, DirInfo* parent);

        //TODO Might remove... Manages filesystem depth
        int getDepth();
        void setDepth(int depth);
        void addDepth(int depth);

        // Manages child nodes
        void addChild(NodeInfo* child);
        void removeChild(NodeInfo* child);
        vector<NodeInfo*> getChildren();
        int getNumChildren();

        // Compare a dir node against itself
        bool similarTo(DirInfo* dir);
        // NodeCompare compareTo(DirInfo* dir);

        // Print dir infor to user
        void printInfo();
};

#endif /* DIRINFO_HPP */

