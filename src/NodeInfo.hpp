/*
 * File: NodeInfo.hpp 
 * Author: ruskiyos
 *
 */

#ifndef NODEINFO_HPP
#define NODEINFO_HPP

// Forward declarations
class DirInfo;

#include <string>
#include <vector>

using namespace std;

class NodeInfo {

    protected:
        string name;
        int size;
        string path;
        DirInfo* parent;
        int modifyTime;
        bool file;
        bool refresh;
        bool starred;
        vector<NodeInfo*> similar;

    public:
        NodeInfo(string name, string path);
        NodeInfo(string name, DirInfo* parent);
        string getName();
        void setName(string name);
        int getSize();
        void setSize(int size);
        void addSize(int size);
        string getPath();
        string getFullPath();
        void updatePath();
        DirInfo* getParent();
        void setParent(DirInfo* parent);
        bool isAncestor(DirInfo* parent);
        bool isFile();
        void isFile(bool isFile);
        bool isDir();
        void isDir(bool isDir);
        bool isStarred();
        void isStarred(bool starred);
        bool isHidden();
        bool getRefresh();
        void setRefresh(bool refresh);
        int getModifyTime();
        void setModifyTime(int time);
        void addSimilar(NodeInfo* node);
        int findSimilar(NodeInfo* node);
        void removeSimilar(NodeInfo* node);
        int getNumSimilar();
        vector<NodeInfo*> getSimilar();

        // Allows children class pointers access to functions
        virtual int getChecksum() {};
        virtual void setChecksum(int sum) {};
        virtual int getDepth() {};
        virtual void setDepth(int depth) {};
        virtual void addChild(NodeInfo* child) {};
        virtual void removeChild(NodeInfo* child) {};
        virtual vector<NodeInfo*> getChildren() {};
        virtual int getNumChildren() {};
        virtual bool similarTo(DirInfo* dir) {};
        //virtual NodeCompare compareTo(DirInfo* dir) {};
};

#endif /* NODEINFO_HPP */

