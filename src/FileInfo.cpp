/*
 * File: FileInfo.cpp 
 * Author: ruskiyos
 *
 */

#include "FileInfo.hpp"

// Checksums
#include <boost/crc.hpp>

#include <exception>  // for std::exception
#include <fstream>    // for std::ifstream
#include <ios>        // for std::ios_base, etc.
#include <iostream>   // for std::cerr, std::cout

// ctor
FileInfo::FileInfo(string name, string path) 
        : NodeInfo(name, path) {

    this->checksum = 0;
}

// ctor
FileInfo::FileInfo(string name, DirInfo* parent)
        : NodeInfo(name, parent) {

    this->checksum = 0;
}

// Calculate file checksum
int FileInfo::getChecksum() {
    if (this->checksum == 0) {
        this->calcChecksum();
    }

    return this->checksum;
}

// Recalculate checksum without giving
// user ability to manually set it
void FileInfo::calcChecksum() {
    boost::crc_32_type result;

    std::ifstream ifs( path.c_str(), std::ios_base::binary );

    if ( ifs ) {
        do {
            char  buffer[1024];

            ifs.read( buffer, 1024 );
            result.process_bytes( buffer, ifs.gcount() );
        } while ( ifs );
    } else {
        cerr << "Failed to open file '" << path << "'." << endl;
    }

    this->checksum = result.checksum();
}

