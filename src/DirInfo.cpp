/*
 * File: DirInfo.cpp 
 * Author: ruskiyos
 *
 */

#include "DirInfo.hpp"

// find()
#include <iostream>
#include <algorithm>


// ctor
DirInfo::DirInfo(string name, string path) 
        : NodeInfo(name, path) {
    this->depth = 0;
    
}

// ctor
DirInfo::DirInfo(string name, DirInfo* parent)
        : NodeInfo(name, parent) {

    this->depth = 0;

}

//TODO might remove... Manages filesystem depth
int DirInfo::getDepth() {
    return this->depth;
}

void DirInfo::setDepth(int depth) {
    this->depth = depth;
}

void DirInfo::addDepth(int depth) {
    this->depth += depth;
}

// Add a child node to its "children" vector
void DirInfo::addChild(NodeInfo* child) {
    children.push_back(child);
}

// Remove a child node from its "children" vector
void DirInfo::removeChild(NodeInfo* child) {
    vector<NodeInfo*>::iterator it;

    //it = find(children.begin(), children.end(), child);

    children.erase(it);
}

// Return all children (vector)
vector<NodeInfo*> DirInfo::getChildren() {
    return this->children;
}

// Count number of children
int DirInfo::getNumChildren() {
    return children.size();
}

void DirInfo::printInfo() {
    vector<NodeInfo*>::iterator it;
    vector<NodeInfo*> sim;

    cout << endl
         << "Directory: " << endl
         << "~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~" << endl
         << "\tPath: " << getPath() << endl
         << "\tSize: " << getSize() << endl
         << "Similars: " << endl;
    
    sim = getSimilar();

    for(it = sim.begin(); it != sim.end(); ++it) {
        cout << "\t" << (*it)->getPath() << " (" 
             << (*it)->getSize() << ")" << endl;
        (*it)->isStarred(true);
    }

}

// Quick directory compare
// If a directory has at least one file that is named
// the same as a file in the current directory, then true
bool DirInfo::similarTo(DirInfo* dir) {
    vector<NodeInfo*>::iterator it;
    vector<NodeInfo*>::iterator temp;

    vector<NodeInfo*> dirs = dir->getChildren();

    for(it = children.begin(); it != children.end(); ++it) {
        for(temp = dirs.begin(); temp != dirs.end(); ++temp) {
            
            if ((*it)->getName() == (*temp)->getName()) {
                return true;
            }
        }
    }
    return false;
}

/**
NodeCompare DirInfo::compareTo(DirInfo* dir) {

}
*/
