/* 
 * File: FileSystem.hpp
 * Author: ruskiyos
 *  
 */

#ifndef FILESYSTEM_HPP
#define FILESYSTEM_HPP

// Forward declarations
class NodeInfo;
class DirInfo;
class FileInfo;

#include <string>
#include <vector>

using namespace std;

class FileSystem {

    protected:
        DirInfo* root;
        string path;
        int depth;
        vector<NodeInfo*> allNodes;
        vector<DirInfo*> allDirs;
        vector<FileInfo*> allFiles;
        int dupDirCount;

    public:
        // To debugPrint or Not To debugPrint
        bool debug;

        FileSystem(string path);
        ~FileSystem();
        DirInfo* getRoot();
        void setRoot(DirInfo* node);
        // Build a DirInfo and FileInfo vector of the filesystem
        bool scan();
        bool scan(DirInfo* node);
        // Update pointers when physically moving a file
        bool moveChild(NodeInfo* child, DirInfo* parent);
        int binarySearch(vector<NodeInfo*> nodes, NodeInfo* node, int min, int max);
        void insertionSort(vector<NodeInfo*> nodes);
        // Used in sorting functions
        static bool sortParam_Size(NodeInfo* a, NodeInfo* b);
        static bool sortParam_Name(NodeInfo* a, NodeInfo* b);
        static bool sortParam_ModifyTime(NodeInfo* a, NodeInfo* b);
        // Find duplicate directories
        bool analyze();
        void displayResults();
        void displayTree(int indent, NodeInfo* node);

        void displayStats();

        // Debug functions
        template<class Mesg> void debugPrint(Mesg mesg);
        template<class NodeVector> void debugPrintNodes(NodeVector nodes); 
};

#endif /* FILESYSTEM_HPP */

