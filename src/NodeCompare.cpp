/*
 * File: NodeCompare.cpp 
 * Author: ruskiyos
 *
 */

#include "NodeCompare.hpp"

using namespace std;

NodeCompare::NodeCompare(NodeInfo* node1, NodeInfo* node2, ScanType type) {
    this->node1 = node1;
    this->node2 = node2;
    this->type = type;
}

vector<NodeInfo*> NodeCompare::getNodeDiff(NodeSelect select) {
    switch(select) {
        case First:
            return diffNode1;
        case Second:
            return diffNode2;
        case Both:
            return sameNode;
        default:
            vector<NodeInfo*> empty;
            return empty;
    }

}

void NodeCompare::addNodeDiff(NodeSelect select, NodeInfo* diff) {
    switch(select) {
        case First:
            diffNode1.push_back(diff);
            break;
        case Second:
            diffNode2.push_back(diff);
            break;
        case Both:
            sameNode.push_back(diff);
            break;
    }
    
}

Status NodeCompare::getStatus() {
    return status;
}

void NodeCompare::setStatus(Status stat) {
    status = stat;
}

ScanType NodeCompare::getScanType() {
    return type;
}

void NodeCompare::setScanType(ScanType stype) {
    type = stype;
}

