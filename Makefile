# File: MakeFile
# Author: ruskiyos
# Project: duplocator
# Created: 11-1-2012

CC = g++
CFLAGS = -Wall -c
SRC = src/
INC = src/
LIBS = -lboost_filesystem -lboost_system
#INC = include/ # might separate includes in future

OBJS = $(SRC)duplocator.o $(SRC)NodeInfo.o $(SRC)DirInfo.o $(SRC)FileInfo.o \
        $(SRC)NodeCompare.o $(SRC)FileSystem.o


all : $(OBJS)
	$(CC) $(OBJS) -o duplocator $(LIBS)

duplocator.o : $(SRC)duplocator.cpp $(INC)FileSystem.hpp
	$(CC) $(CFLAGS) $(SRC)$<

NodeInfo.o : $(SRC)NodeInfo.cpp $(INC)NodeInfo.hpp $(INC)DIrInfo.hpp
	$(CC) $(CFLAGS) $(SRC)$<

DirInfo.o : $(INC)DirInfo.hpp $(SRC)DirInfo.cpp $(INC)NodeInfo.hpp
	$(CC) -c $(SRC)DirInfo.cpp

FileInfo.o : $(INC)FileInfo.hpp $(SRC)FileInfo.cpp $(INC)NodeInfo.hpp
	$(CC) -c $(SRC)FileInfo.cpp

NodeCompare.o : $(SRC)NodeCompare.cpp $(INC)NodeCompare.hpp $(INC)NodeInfo.hpp
	$(CC) $(CFLAGS) $(SRC)$<

FileSystem.o : $(SRC)FileSystem.cpp $(INC)FileSystem.hpp $(INC)NodeInfo.hpp \
        $(INC)FileInfo.hpp $(INC)DirInfo.hpp
	$(CC) $(CFLAGS) $(SRC)$<

clean :
	rm -f $(OBJS) duplocator
